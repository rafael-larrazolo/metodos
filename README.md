# README #


Repositorio para el proyecto final de métodos analíticos.

La idea es hacer un chatbot y utilizar una interfaz (Telegram) para interactuar con el mismo.

### Tutorial para Telegram ###

En esta [liga](https://www.codementor.io/garethdwyer/building-a-telegram-bot-using-python-part-1-goi5fncay) está el tutorial que hice para ver cómo hacer un bot con Telegram.
Siento que está bastante fácil e intuitivo.



La idea es poder hacer la conexión de nuestro bot en telegram con un código en Python el cual va a contener las respuestas del chatbot en Python (el cual funciona con keras).

En este [sitio](https://chatbotsmagazine.com/contextual-chat-bots-with-tensorflow-4391749d0077) encontré cómo hacer el chatbot en Python.



NOTA: ya logré hacer la conexión entre python (el modelo) y el bot (telegram). Sólo falta conseguir los datos, limpiarlos y que el bot aprenda...